/*
 * The MIT License
 *
 * 1Jar
 *
 * Copyright 2015 Ibbtek <http://ibbtek.altervista.org/>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.ibbtek.onejar;

import static com.ibbtek.onejar.MainGui.jLabelInfos;
import static com.ibbtek.onejar.MainGui.jProgressBar1;
import static com.ibbtek.onejar.MainGui.libraries;
import static com.ibbtek.onejar.MainGui.mainJar;
import static com.ibbtek.onejar.MainGui.splashScreen;
import static com.ibbtek.utilities.LogToFile.log;
import com.ibbtek.utilities.TmpDir;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.Enumeration;
import java.util.jar.Attributes;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;
import java.util.jar.Manifest;
import javax.swing.JOptionPane;

/**
 * JarPackager class
 *
 * @author Ibbtek <ibbtek@gmail.com>
 */
public class JarPackager implements Runnable {

    TmpDir tmpDir=null;
    Path tmpDirPath;
    File targetJar = null;

    @Override
    public void run() {
        try {
            //Create tmp directory
            tmpDir = new TmpDir("1JAR-tmp");
            tmpDirPath = tmpDir.getPath();
            tmpDir.deleteOnExit();
            //Extracting Main Jar
            jProgressBar1.setValue(0);
            jLabelInfos.setText("Extracting Main Jar...");
            extJar(mainJar, tmpDirPath.toFile());
            
            //Extracting libraries
            if (!libraries.isEmpty()) {
                for (JarFile file : libraries) {
                    /*
                    Increment the progress bar
                    */
                    jLabelInfos.setText("Extracting Jar library #"
                            + (libraries.indexOf(file) + 1) + "...");
                    jProgressBar1.setValue(libraries.indexOf(file) + 1);
                    extJar(file, tmpDirPath.toFile());
                
                }
            }
            //Copying splash screen
            if (splashScreen != null) {
                File outputSplash = new File(tmpDirPath + "/META-INF/" + splashScreen.getName());
                copyFile(splashScreen, outputSplash);
                
            }
            
            //Create new manifest
            mkManif(tmpDirPath.toFile(), getMainClass());
            
            
            //Compiling the new jar
            jLabelInfos.setText("Compiling the new Jar...");
            jProgressBar1.setValue(libraries.size() + 1);
            targetJar = new File(mainJar.getName() + "-1JAR-.jar");
            jarDirectory(tmpDirPath.toFile(), targetJar);
            
        } catch (IOException | InterruptedException ex) {
            log(ex, "severe", "Process aborted, check the log file.");
            Thread.currentThread().interrupt();
            if (targetJar != null) {
                targetJar.delete();
            }
        } finally {
            if(tmpDir!=null)
                tmpDir.delete();
        }
        jLabelInfos.setText("Compilation finished.");
        jProgressBar1.setValue(libraries.size() + 2);
        JOptionPane.showMessageDialog(null, "Packaging successful.",
                    "Info", JOptionPane.INFORMATION_MESSAGE);
    }

    /**
     *
     * @param directory
     * @param jar
     * @throws IOException
     * @throws java.lang.InterruptedException
     */
    public void jarDirectory(File directory, File jar)
            throws IOException, InterruptedException {
        try (JarOutputStream jos = new JarOutputStream(new FileOutputStream(jar))) {
            jar(directory, directory, jos);
        }
    }

    /**
     *
     * @param directory
     * @param base
     * @param jos
     * @throws IOException
     */
    private void jar(File directory, File base,
            JarOutputStream jos) throws IOException, InterruptedException {
        File[] files = directory.listFiles();
        byte[] buffer = new byte[1024];
        int bytesread = 0;
        for (int i = 0, n = files.length; i < n; i++) {
            if (Thread.interrupted()) {
                throw new InterruptedException();
            }
            if (files[i].isDirectory()) {
                jar(files[i], base, jos);
            } else {
                try (FileInputStream in = new FileInputStream(files[i])) {
                    JarEntry entry = new JarEntry(files[i].getPath().substring(
                            base.getPath().length() + 1).replace("\\", "/"));
                    entry.setMethod(JarEntry.DEFLATED);
                    entry.setSize((long) buffer.length);
                    jos.putNextEntry(entry);
                    while (-1 != (bytesread = in.read(buffer))) {
                        jos.write(buffer, 0, bytesread);
                    }
                }
            }
        }
    }

    /**
     *
     * @param dir
     * @param mainClass
     * @throws IOException
     */
    private void mkManif(File dir, String mainClass) throws IOException {
        String str = "Manifest-Version: 1.0\nMain-Class: " + mainClass + "\n";
        if (splashScreen != null) {
            str = str + "SplashScreen-Image: META-INF/" + splashScreen.getName() + "\n";
        }
        File manifest = new File(dir + "/META-INF/MANIFEST.MF");
        FileWriter fw;
        fw = new FileWriter(manifest);
        fw.write(str);
        fw.close();
    }

    /**
     *
     * @param jarPath
     * @param dir
     * @throws IOException
     */
    private void extJar(JarFile jar, File dir) throws IOException, InterruptedException {
        Enumeration enu = jar.entries();
        while (enu.hasMoreElements()) {
            if (Thread.interrupted()) {
                throw new InterruptedException();
            }
            JarEntry file = (JarEntry) enu.nextElement();
            File f = new File(dir + File.separator + file.getName());
            if (!f.exists()) {
                f.getParentFile().mkdirs();
                f = new java.io.File(dir + File.separator + file.getName());
            }
            if (file.isDirectory()) {
                continue;
            }
            try (InputStream is = jar.getInputStream(file) // get the input stream
                    ; FileOutputStream fos = new FileOutputStream(f)) {
                while (is.available() > 0) {  // write contents of 'is' to 'fos'
                    fos.write(is.read());
                }
            }
        }
    }

    /**
     *
     * @param inputFile
     * @param outputFile
     * @throws FileNotFoundException
     * @throws IOException
     */
    private void copyFile(File inputFile, File outputFile) throws FileNotFoundException, IOException {
        FileOutputStream out;
        try (FileInputStream in = new FileInputStream(inputFile)) {
            out = new FileOutputStream(outputFile);
            int c;
            while ((c = in.read()) != -1) {
                out.write(c);
            }
        }
        out.close();
    }

    /**
     *
     * @return @throws IOException
     */
    private String getMainClass() throws IOException {
        Manifest manifest;
        manifest = mainJar.getManifest();
        return manifest.getMainAttributes().getValue(Attributes.Name.MAIN_CLASS);
    }
}
